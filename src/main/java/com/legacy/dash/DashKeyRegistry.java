package com.legacy.dash;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.client.KeyMapping;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.ClientRegistry;

@OnlyIn(Dist.CLIENT)
public class DashKeyRegistry
{
	List<KeyMapping> keys;

	public DashKeyRegistry()
	{
		this.keys = new ArrayList<KeyMapping>();
	}

	public DashKeyRegistry register(KeyMapping keyBinding)
	{
		if (!keys.contains(keyBinding))
		{
			this.keys.add(keyBinding);
			ClientRegistry.registerKeyBinding(keyBinding);
		}

		return this;
	}
}
