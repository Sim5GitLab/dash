package com.legacy.dash;

import net.minecraft.client.Minecraft;
import net.minecraft.world.item.enchantment.EnchantmentHelper;
import net.minecraft.world.entity.player.Player;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.client.event.InputEvent;
import net.minecraftforge.event.TickEvent.ClientTickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class DashEntityEvents
{
	byte cooldown = 0;

	@SubscribeEvent
	public void onKeyPressed(InputEvent.KeyInputEvent event)
	{
		Minecraft mc = Minecraft.getInstance();

		if (mc.player == null)
			return;

		Player player = mc.player;

		float enchantmentLevel = EnchantmentHelper.getEnchantmentLevel(DashRegistry.DASHING, player);

		if (DashMod.DashClient.DASH_KEYBIND.isDown() && cooldown <= 0 && enchantmentLevel > 0 && player.zza != 0 && !player.isInWaterOrBubble())
		{
			for (int i = 0; i < 20; ++i)
			{
				double d0 = mc.level.random.nextGaussian() * 0.02D;
				double d1 = mc.level.random.nextGaussian() * 0.02D;
				double d2 = mc.level.random.nextGaussian() * 0.02D;
				mc.level.addParticle(ParticleTypes.POOF, player.getX() + (double) (player.level.random.nextFloat() * player.getBbWidth() * 2.0F) - (double) player.getBbWidth() - d0 * 10.0D, player.getY() + (double) (player.level.random.nextFloat() * player.getBbHeight()) - d1 * 10.0D, player.getZ() + (double) (player.level.random.nextFloat() * player.getBbWidth() * 2.0F) - (double) player.getBbWidth() - d2 * 10.0D, d0, d1, d2);
			}

			Vec3 playerLook = player.getViewVector(1);
			Vec3 dashVec = new Vec3(playerLook.x(), player.getDeltaMovement().y(), playerLook.z());
			player.setDeltaMovement(dashVec);

			player.playSound(SoundEvents.PHANTOM_FLAP, 1.0F, 2.0F);
			cooldown = 50;
		}
	}

	@SubscribeEvent
	public void onClientTick(ClientTickEvent event)
	{
		if (cooldown > 0)
			--cooldown;
	}
}
